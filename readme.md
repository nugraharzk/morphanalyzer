# Morphological Analyzer

Dibuat untuk memenuhi salah satu tugas Natural Language Processing (NLP) Ilmu Komputer UPI 2018.

Oleh :
- Burhanudin (1504286)
- Rizki Nugraha (1506748)

## Cara Menggunakan Aplikasi Ini
1. Clone terlebih dahulu ke dalam mesin Anda.
    ``` sh
    $ git clone https://gitlab.com/nugraharzk/morphanalyzer.git morph
    ```
2. Karena aplikasi ini menggunakan framework CodeIgniter, maka pindahkan ke dalam direktori htdocs jika Anda menggunakan XAMPP di Windows.
    ``` sh
    $ move morph C:\xampp\htdocs
    ```
3. Jika Anda menggunakan Linux:
    ``` sh
    $ mv morph/ /var/www/html/
    ```
4. Buat database baru dengan nama database dibawah ini, lalu jalankan .sql yang terdapat pada root folder morph.
    ``` sh
    db_morph
    ```
5. Buka browser Anda, lalu ketikan pada kolom alamat:
    ``` sh
    http://localhost/morph
    ```

