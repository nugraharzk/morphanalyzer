<?php

class M_Morph extends CI_Model 
{
    public function __construct()
	{
		parent::__construct();
    }

    public function getKata()
    {
        return $this->db->select('*')->get('tb_katadasar')->result();
    }
    
    public function findKata($kata)
    {
        $q = $this->db->select('*')->where('katadasar',$kata)->get('tb_katadasar');
        
        if ($q != null) {
            return $q->result();
        } else {
            return false;
        }
    }

}
