<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="<?=base_url('assets/css/bootstrap.css');?>">
    <link rel="stylesheet" href="<?= base_url('assets/css/flat-ui.css') ?>">
    <title>Morphological Analyzer Result</title>
    <style>
        body {
            background-color: #ECF0F1;
        }
        .container {
            margin: 100px;
        }
        h4 {
            text-align: center;
        }
        a {
            color: white;
        }
        .notfound {
            text-align: center;
        }
    </style>
</head>
<body>
    <div class="container">
        <h4>Hasilnya adalah</h4>
        <table border="4px" class="table table-bordered table-hover">
            <thead>
                <td><b>Kata</b></td>
                <td><b>Kata Dasar</b></td>
                <td><b>Jenis</b></td>
            </thead>
            <?php if ($data) { ?>
            <?php foreach ($data as $value) { ?>
            <tbody>
                <td><?= $nama ?></td>
                <td><?= $value->katadasar; ?></td>
                <td><?= $value->tipe_katadasar; ?></td>
            </tbody>    
            <?php } ?>
            <?php } else { ?>
            <tbody>
                <td colspan="3" class="notfound"><b>Hasil tidak ditemukan</b></td>
            </tbody>
            <?php } ?>
        </table>
        <div class="col">
            <div class="form-group">
                <a href="<?= site_url('Morph'); ?>" class="btn btn-warning form-control">Kembali</a><br><br>
                <a href="<?= base_url(); ?>" class="btn btn-primary form-control">Ke Navigasi Awal</a>
            </div>
        </div>
    </div>    
</body>
</html>