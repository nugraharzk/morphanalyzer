<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="<?=base_url('assets/css/bootstrap.css');?>">
    <link rel="stylesheet" href="<?= base_url('assets/css/flat-ui.css') ?>">
    <title>Morphological Analyzer Result</title>
    <style>
        body {
            background-color: #ECF0F1;
        }
        .container {
            margin: 100px;
        }
        h4 {
            text-align: center;
        }
        a {
            color: white;
        }
        .notfound {
            text-align: center;
        }
    </style>
</head>
<body>
    <div class="container">
        <h4>Kata Inputnya adalah : <?php echo $input ?></h4>
        <table border="4px" class="table table-bordered table-hover">
            <thead>
                <td><b>No</b></td>
                <td><b>Kata Dasar Relevan</b></td>
                <td><b>Nilai Levenshtein</b></td>
                <td><b>Jenis</b></td>
            </thead>
            <?php $index = 1; if ($data) { ?>
            <?php for ($i = 0; $i < $jumlah; $i++) { ?>
            <tbody>
                <td><?= $index ?></td>
                <td><?= $hasil[$i]->katadasar; ?></td>
                <td><?= $nilai[$i]; ?></td>
                <td><?= $hasil[$i]->tipe_katadasar; ?></td>
            </tbody>    
            <?php $index++; } ?>
            <?php } else { ?>
            <tbody>
                <td colspan="3" class="notfound"><b>Hasil tidak ditemukan</b></td>
            </tbody>
            <?php } ?>
        </table>
        <div class="col">
            <div class="form-group">
                <a href="<?= site_url('Levenshtein'); ?>" class="btn btn-warning form-control">Kembali</a><br><br>
                <a href="<?= base_url(); ?>" class="btn btn-primary form-control">Ke Navigasi Awal</a>
            </div>
        </div>
    </div>    
</body>
</html>