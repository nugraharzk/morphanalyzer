<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="<?=base_url('assets/css/bootstrap.css');?>">
    <link rel="stylesheet" href="<?= base_url('assets/css/flat-ui.css') ?>">
    <style>
        body {
            background-color: #ECF0F1;
        }
        h4 {
            text-align: center;
            color: #999999;
        }
        h2 {
            text-align: center;
            margin-top: 200px;
            color: #000000;
        }
        input {
            width: calc(100%);
            border-radius: 10px;
            padding: 20px;
            background-color: #3498DB;
            color: white;
        }
    </style>
    <title>Levenshtein Distance</title>
</head>
<body>
    <div class="container">
        <h2>Levenshtein Distance</h2>
        <form action="<?= site_url('Levenshtein/cariJarak') ?>" method="POST">
            <div class="col">
                <div class="form-group">
                    <input type="text" placeholder="Masukkan kata disini" name="input" class="form-control">
                </div>
                <div class="form-group">
                    <input type="submit" value="Cari!" name="submit" class="btn btn-inverse"><br><br>
                <a href="<?= base_url(); ?>" class="btn btn-primary form-control">Ke Navigasi Awal</a>
                </div>
            </div>
        </form>
    </div>
</body>
</html>