<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="<?=base_url('assets/css/bootstrap.css');?>">
    <link rel="stylesheet" href="<?= base_url('assets/css/flat-ui.css') ?>">
    <style>
        body {
            background-color: #ECF0F1;
        }
        h4 {
            text-align: center;
            color: #999999;
        }
        h2 {
            text-align: center;
            margin-top: 200px;
            color: #000000;
        }
        input {
            width: calc(100%);
            border-radius: 10px;
            padding: 20px;
            background-color: #3498DB;
            color: white;
        }
    </style>
    <title>Levenshtein Distance</title>
</head>
<body>
    <div class="container">
        <h2>Fitur Belum Tersedia</h2>
        <h4>Datang kembali lain kali.</h4><br><br>
        <a href="<?= base_url(); ?>" class="d-flex justify-content-center btn btn-primary">Ke Navigasi Awal</a>
    </div>
</body>
</html>