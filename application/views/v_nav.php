<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="<?=base_url('assets/css/bootstrap.css');?>">
    <link rel="stylesheet" href="<?= base_url('assets/css/flat-ui.css') ?>">
    <style>
        body {
            background-color: #ECF0F1;
            margin: 15%;
        }
        a {
            align-items: center;
            padding: 40%;
            margin: 5px;
        }
        h4 {
            text-align: center;
            color: #999999;
        }
    </style>
    <title>Natural Language Processing</title>
</head>
<body>
    <h1 class="d-flex justify-content-center">Natural Language Processing</h1>
    <h4>Oleh Burhanudin (1504286) dan Rizki Nugraha (1506748)</h4><br>
    <div class="d-flex justify-content-center">
        <a href="<?php echo site_url('Morph') ?>" class="btn btn-success col-md-3">Morphological Analyzer</a>
        <a href="<?php echo site_url('Levenshtein') ?>" class="btn btn-success col-md-3">Levenshtein Distance</a>
        <a href="<?php echo site_url('Langmodel') ?>" class="btn btn-success col-md-3">Language Model</a>
    </div>
</body>
</html>