<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Morph extends CI_Controller {

	public function index()
	{
		$this->load->view('v_morph');
	}
	
	public function showKata()
	{
		$result['nama'] = $this->input->post('nama');
		$result['hasilcari'] = $this->cariKata($result['nama'],0);
		$result['data'] = $this->M_Morph->findKata($result['hasilcari'],0);
		// print_r($result['data']);
		$this->load->view('v_result',$result);
	}
	
	public function cariKata($kata,$i)
	{
		if ($this->cekKamus($kata)) {
			return $kata;
		} else if ($i < 45) { //JUMLAH INDEX MAKSIMAL
			$kata = $this->deletePrefix($kata,$i);
			
			if ($this->cekKamus($kata) == true) {
				return $kata;
			} else {
				return $this->cariKata($kata,$i+1);
			}
			
		} else if ($this->cekKamus($kata)){
			return $kata;
		}
		// else if ($i == 1) {
			// $kata = $this->deleteSuffix($kata);
			// return $kata;
			// if ($this->cekKamus($kata) == true) {
				// return $kata;
			// } else {
				// return $this->cariKata($kata,2);
				// return $kata;
			// }
		// }
		// } else if ($i == 2) {
		// 	$kata = $this->deleteSuffix($kata);
			
		// 	if ($this->cekKamus($kata) == true) {
		// 		return $kata;
		// 	} else {
		// 		$this->findKata($kata,2);
		// 	}
		// $kata = $this->deletePrefix($kata);
		// return $kata;
	}
	
	public function cekKamus($kata)
	{
		if ($this->M_Morph->findKata($kata)) {
			return true;
		} else {
			return false;
		}
	}

	public function deletePrefix($kata,$i){
		$kataAsal = $kata;
		
		/* —— Tentukan Tipe Awalan ————*/
		if($i == 0){ //meng
			if(preg_match('/^(meng[aeghiuo])/',$kata)){
				$__kata = preg_replace('/^(meng)/','',$kata);
				
				if($this->cekKamus($__kata)){
					return $__kata; // Jika ada balik
				}
			}
		}else if($i == 1){ //meng + suffix(tanpa 'an')
			if(preg_match('/^(meng[aeghiuo])/',$kata)){
				$__kata = preg_replace('/^(meng)/','',$kata);

				if(preg_match('/(kan|i|nya|ku|mu)\b/',$__kata)){ // Cek Suffixes
					$__kata = preg_replace('/(kan|i|nya|ku|mu)\b/','',$__kata);				
					if($this->cekKamus($__kata)){
						return $__kata; // Jika ada balik
					}
				}
			}
		}else if($i == 2){ //meng(k)
			if(preg_match('/^(meng[aeiuo])/',$kata)){
				$__kata = preg_replace('/^(meng)/','k',$kata);
				
				if($this->cekKamus($__kata)){
					return $__kata; // Jika ada balik
				}
			}
		}else if($i == 3){ //meng(k) + suffix(tanpa 'an')
			if(preg_match('/^(meng[aeiuo])/',$kata)){
				$__kata = preg_replace('/^(meng)/','k',$kata);

				if(preg_match('/(kan|i|nya|ku|mu)\b/',$__kata)){ // Cek Suffixes
					$__kata = preg_replace('/(kan|i|nya|ku|mu)\b/','',$__kata);				
					if($this->cekKamus($__kata)){
						return $__kata; // Jika ada balik
					}
				}
			}
		}else if($i == 4){ //peng
			if(preg_match('/^(peng[aeghiuo])/',$kata)){
				$__kata = preg_replace('/^(peng)/','',$kata);
				
				if($this->cekKamus($__kata)){
					return $__kata; // Jika ada balik
				}
			}
		}else if($i == 5){ //peng + suffix(tanpa 'kan')
			if(preg_match('/^(peng[aeghiuo])/',$kata)){
				$__kata = preg_replace('/^(peng)/','',$kata);

				if(preg_match('/(an|i|nya|ku|mu)\b/',$__kata)){ // Cek Suffixes
					$__kata = preg_replace('/(an|i|nya|ku|mu)\b/','',$__kata);				
					if($this->cekKamus($__kata)){
						return $__kata; // Jika ada balik
					}
				}
			}
		}else if($i == 6){ //peng(k)
			if(preg_match('/^(peng[aeiuo])/',$kata)){
				$__kata = preg_replace('/^(peng)/','k',$kata);
				
				if($this->cekKamus($__kata)){
					return $__kata; // Jika ada balik
				}
			}
		}else if($i == 7){ //peng(k) + suffix(tanpa 'kan')
			if(preg_match('/^(peng[aeiuo])/',$kata)){ // Jika di-,ke-,se-
				$__kata = preg_replace('/^(peng)/','k',$kata);

				if(preg_match('/(an|i|nya|ku|mu)\b/',$__kata)){ // Cek Suffixes
					$__kata = preg_replace('/(an|i|nya|ku|mu)\b/','',$__kata);				
					if($this->cekKamus($__kata)){
						return $__kata; // Jika ada balik
					}
				}
			}
		}else if($i == 8){ //meny
			if(preg_match('/^(meny[aeiuo])/',$kata)){
				$__kata = preg_replace('/^(meny)/','s',$kata);
				
				if($this->cekKamus($__kata)){
					return $__kata; // Jika ada balik
				}
			}
		}else if($i == 9){ //meny + suffix(tanpa 'an')
			if(preg_match('/^(meny[aeiuo])/',$kata)){ // Jika di-,ke-,se-
				$__kata = preg_replace('/^(meny)/','s',$kata);

				if(preg_match('/(kan|i|nya|ku|mu)\b/',$__kata)){ // Cek Suffixes
					$__kata = preg_replace('/(kan|i|nya|ku|mu)\b/','',$__kata);				
					if($this->cekKamus($__kata)){
						return $__kata; // Jika ada balik
					}
				}
			}
		}else if($i == 10){ //peny
			if(preg_match('/^(peny[aeiuo])/',$kata)){
				$__kata = preg_replace('/^(peny)/','s',$kata);
				
				if($this->cekKamus($__kata)){
					return $__kata; // Jika ada balik
				}
			}
		}else if($i == 11){ //peny + suffix(tanpa 'kan')
			if(preg_match('/^(peny[aeiuo])/',$kata)){ // Jika di-,ke-,se-
				$__kata = preg_replace('/^(peny)/','s',$kata);

				if(preg_match('/(an|i|nya|ku|mu)\b/',$__kata)){ // Cek Suffixes
					$__kata = preg_replace('/(an|i|nya|ku|mu)\b/','',$__kata);				
					if($this->cekKamus($__kata)){
						return $__kata; // Jika ada balik
					}
				}
			}
		}else if($i == 12){ //men
			if(preg_match('/^(men[cdjz])/',$kata)){
				$__kata = preg_replace('/^(men)/','',$kata);
				
				if($this->cekKamus($__kata)){
					return $__kata; // Jika ada balik
				}
			}
		}else if($i == 13){ //men + suffix(tanpa 'an')
			if(preg_match('/^(men[cdjz])/',$kata)){
				$__kata = preg_replace('/^(men)/','',$kata);

				if(preg_match('/(kan|i|nya|ku|mu)\b/',$__kata)){ // Cek Suffixes
					$__kata = preg_replace('/(kan|i|nya|ku|mu)\b/','',$__kata);				
					if($this->cekKamus($__kata)){
						return $__kata; // Jika ada balik
					}
				}
			}
		}else if($i == 14){ //men(t)
			if(preg_match('/^(men[aeiuo])/',$kata)){
				$__kata = preg_replace('/^(men)/','t',$kata);
				
				if($this->cekKamus($__kata)){
					return $__kata; // Jika ada balik
				}
			}
		}else if($i == 15){ //men(t) + suffix(tanpa 'an')
			if(preg_match('/^(men[aeiuo])/',$kata)){
				$__kata = preg_replace('/^(men)/','t',$kata);

				if(preg_match('/(kan|i|nya|ku|mu)\b/',$__kata)){ // Cek Suffixes
					$__kata = preg_replace('/(kan|i|nya|ku|mu)\b/','',$__kata);				
					if($this->cekKamus($__kata)){
						return $__kata; // Jika ada balik
					}
				}
			}
		}else if($i == 16){ //pen
			if(preg_match('/^(pen[cdjz])/',$kata)){
				$__kata = preg_replace('/^(pen)/','',$kata);
				
				if($this->cekKamus($__kata)){
					return $__kata; // Jika ada balik
				}
			}
		}else if($i == 17){ //pen + suffix(tanpa 'kan')
			if(preg_match('/^(pen[cdjz])/',$kata)){
				$__kata = preg_replace('/^(pen)/','',$kata);

				if(preg_match('/(an|i|nya|ku|mu)\b/',$__kata)){ // Cek Suffixes
					$__kata = preg_replace('/(an|i|nya|ku|mu)\b/','',$__kata);				
					if($this->cekKamus($__kata)){
						return $__kata; // Jika ada balik
					}
				}
			}
		}else if($i == 18){ //pen(t)
			if(preg_match('/^(pen[aeiuo])/',$kata)){
				$__kata = preg_replace('/^(pen)/','t',$kata);
				
				if($this->cekKamus($__kata)){
					return $__kata; // Jika ada balik
				}
			}
		}else if($i == 19){ //pen(t) + suffix(tanpa 'kan')
			if(preg_match('/^(pen[aeiuo])/',$kata)){ // Jika di-,ke-,se-
				$__kata = preg_replace('/^(pen)/','t',$kata);

				if(preg_match('/(an|i|nya|ku|mu)\b/',$__kata)){ // Cek Suffixes
					$__kata = preg_replace('/(an|i|nya|ku|mu)\b/','',$__kata);				
					if($this->cekKamus($__kata)){
						return $__kata; // Jika ada balik
					}
				}
			}
		}else if($i == 20){ //mem
			if(preg_match('/^(mem[bfv])/',$kata)){
				$__kata = preg_replace('/^(mem)/','',$kata);
				
				if($this->cekKamus($__kata)){
					return $__kata; // Jika ada balik
				}
			}
		}else if($i == 21){ //mem + suffix(tanpa 'an')
			if(preg_match('/^(mem[bfv])/',$kata)){
				$__kata = preg_replace('/^(mem)/','',$kata);

				if(preg_match('/(kan|i|nya|ku|mu)\b/',$__kata)){ // Cek Suffixes
					$__kata = preg_replace('/(kan|i|nya|ku|mu)\b/','',$__kata);				
					if($this->cekKamus($__kata)){
						return $__kata; // Jika ada balik
					}
				}
			}
		}else if($i == 22){ //mem(p)
			if(preg_match('/^(mem[aeiuo])/',$kata)){
				$__kata = preg_replace('/^(mem)/','p',$kata);
				
				if($this->cekKamus($__kata)){
					return $__kata; // Jika ada balik
				}
			}
		}else if($i == 23){ //mem(p) + suffix(tanpa 'an')
			if(preg_match('/^(mem[aeiuo])/',$kata)){
				$__kata = preg_replace('/^(mem)/','p',$kata);

				if(preg_match('/(kan|i|nya|ku|mu)\b/',$__kata)){ // Cek Suffixes
					$__kata = preg_replace('/(kan|i|nya|ku|mu)\b/','',$__kata);				
					if($this->cekKamus($__kata)){
						return $__kata; // Jika ada balik
					}
				}
			}
		}else if($i == 24){ //pem
			if(preg_match('/^(pem[bfv])/',$kata)){
				$__kata = preg_replace('/^(pem)/','',$kata);
				
				if($this->cekKamus($__kata)){
					return $__kata; // Jika ada balik
				}
			}
		}else if($i == 25){ //pem + suffix(tanpa 'i|kan')
			if(preg_match('/^(pem[bfv])/',$kata)){
				$__kata = preg_replace('/^(pem)/','',$kata);

				if(preg_match('/(an|nya|ku|mu)\b/',$__kata)){ // Cek Suffixes
					$__kata = preg_replace('/(an|i|nya|ku|mu)\b/','',$__kata);				
					if($this->cekKamus($__kata)){
						return $__kata; // Jika ada balik
					}
				}
			}
		}else if($i == 26){ //pem(p)
			if(preg_match('/^(pem[aeiuo])/',$kata)){
				$__kata = preg_replace('/^(pem)/','p',$kata);
				
				if($this->cekKamus($__kata)){
					return $__kata; // Jika ada balik
				}
			}
		}else if($i == 27){ //pem(p) + suffix(tanpa 'i|kan')
			if(preg_match('/^(pem[aeiuo])/',$kata)){ // Jika di-,ke-,se-
				$__kata = preg_replace('/^(pem)/','p',$kata);

				if(preg_match('/(an|nya|ku|mu)\b/',$__kata)){ // Cek Suffixes
					$__kata = preg_replace('/(an|i|nya|ku|mu)\b/','',$__kata);				
					if($this->cekKamus($__kata)){
						return $__kata; // Jika ada balik
					}
				}
			}
		}else if($i == 28){ //me
			if(preg_match('/^(me[lmnrwy])/',$kata)){
				$__kata = preg_replace('/^(me)/','',$kata);
				
				if($this->cekKamus($__kata)){
					return $__kata; // Jika ada balik
				}
			}
		}else if($i == 29){ //me + suffix(tanpa 'an')
			if(preg_match('/^(me[lmnrwy])/',$kata)){
				$__kata = preg_replace('/^(me)/','',$kata);

				if(preg_match('/(kan|i|nya|ku|mu)\b/',$__kata)){ // Cek Suffixes
					$__kata = preg_replace('/(kan|i|nya|ku|mu)\b/','',$__kata);				
					if($this->cekKamus($__kata)){
						return $__kata; // Jika ada balik
					}
				}
			}
		}else if($i == 30){ //pe
			if(preg_match('/^(pe[lmnrwy])/',$kata)){
				$__kata = preg_replace('/^(pe)/','',$kata);
				
				if($this->cekKamus($__kata)){
					return $__kata; // Jika ada balik
				}
			}
		}else if($i == 31){ //pe + suffix(tanpa 'an')
			if(preg_match('/^(pe[lmnrwy])/',$kata)){
				$__kata = preg_replace('/^(pe)/','',$kata);

				if(preg_match('/(kan|i|nya|ku|mu)\b/',$__kata)){ // Cek Suffixes
					$__kata = preg_replace('/(kan|i|nya|ku|mu)\b/','',$__kata);				
					if($this->cekKamus($__kata)){
						return $__kata; // Jika ada balik
					}
				}
			}
		}else if($i == 32){ //per + (an)
			if(preg_match('/^(per[abcdefghijklmnopqstuvwxyz])/',$kata)){
				$__kata = preg_replace('/^(per)/','',$kata);

				if(preg_match('/(an)\b/',$__kata)){ // Cek Suffixes
					$__kata = preg_replace('/(an)\b/','',$__kata);				
					if($this->cekKamus($__kata)){
						return $__kata; // Jika ada balik
					}
				}
			}
		}else if($i == 33){ //per(r) + (an)
			if(preg_match('/^(per[aeiou])/',$kata)){
				$__kata = preg_replace('/^(per)/','r',$kata);

				if(preg_match('/(an)\b/',$__kata)){ // Cek Suffixes
					$__kata = preg_replace('/(an)\b/','',$__kata);				
					if($this->cekKamus($__kata)){
						return $__kata; // Jika ada balik
					}
				}
			}
		}else if($i == 34){ //ber
			if(preg_match('/^(ber[abcdefghijklmnopqstuvwxyz])/',$kata)){
				$__kata = preg_replace('/^(ber)/','',$kata);

				if($this->cekKamus($__kata)){
					return $__kata; // Jika ada balik
				}
			}
		}else if($i == 35){ //ber(r)
			if(preg_match('/^(ber[aeiou])/',$kata)){
				$__kata = preg_replace('/^(ber)/','r',$kata);

				if($this->cekKamus($__kata)){
					return $__kata; // Jika ada balik
				}
			}
		}else if($i == 36){ //ber + an|kan
			if(preg_match('/^(ber[abcdefghijklmnopqstuvwxyz])/',$kata)){
				$__kata = preg_replace('/^(ber)/','',$kata);

				if(preg_match('/(kan|an)\b/',$__kata)){ // Cek Suffixes
					$__kata = preg_replace('/(kan|an)\b/','',$__kata);				
					if($this->cekKamus($__kata)){
						return $__kata; // Jika ada balik
					}
				}
			}
		}else if($i == 37){ //ber(r) + an|kan
			if(preg_match('/^(ber[aeiou])/',$kata)){
				$__kata = preg_replace('/^(ber)/','r',$kata);

				if(preg_match('/(kan|an)\b/',$__kata)){ // Cek Suffixes
					$__kata = preg_replace('/(kan|an)\b/','',$__kata);				
					if($this->cekKamus($__kata)){
						return $__kata; // Jika ada balik
					}
				}
			}
		}else if($i == 38){ //ter
			if(preg_match('/^(ter[abcdefghijklmnopqstuvwxyz])/',$kata)){
				$__kata = preg_replace('/^(ter)/','',$kata);

				if($this->cekKamus($__kata)){
					return $__kata; // Jika ada balik
				}
			}
		}else if($i == 39){ //ter(r)
			if(preg_match('/^(ter[aeiou])/',$kata)){
				$__kata = preg_replace('/^(ter)/','r',$kata);

				if($this->cekKamus($__kata)){
					return $__kata; // Jika ada balik
				}
			}
		}else if($i == 40){ //di|se
			if(preg_match('/^(di|se)/',$kata)){
				$__kata = preg_replace('/^(di)/','',$kata);

				if($this->cekKamus($__kata)){
					return $__kata; // Jika ada balik
				}
			}
		}else if($i == 41){ //di + suffix
			if(preg_match('/^(di)/',$kata)){
				$__kata = preg_replace('/^(di)/','',$kata);

				if(preg_match('/(kan|i|nya)\b/',$__kata)){ // Cek Suffixes
					$__kata = preg_replace('/(kan|i|nya)\b/','',$__kata);				
					if($this->cekKamus($__kata)){
						return $__kata; // Jika ada balik
					}
				}
			}
		}else if($i == 42){ //se + suffix
			if(preg_match('/^(se)/',$kata)){
				$__kata = preg_replace('/^(se)/','',$kata);

				if(preg_match('/(nya|ku|mu)\b/',$__kata)){ // Cek Suffixes
					$__kata = preg_replace('/(nya|ku|mu)\b/','',$__kata);				
					if($this->cekKamus($__kata)){
						return $__kata; // Jika ada balik
					}
				}
			}
		}else if($i == 43){ //ke + suffix
			if(preg_match('/^(ke)/',$kata)){
				$__kata = preg_replace('/^(se)/','',$kata);

				if(preg_match('/(an)\b/',$__kata)){ // Cek Suffixes
					$__kata = preg_replace('/(an)\b/','',$__kata);				
					if($this->cekKamus($__kata)){
						return $__kata; // Jika ada balik
					}
				}
			}
		}else if($i == 44){
			if(preg_match('/(kan|i|an|nya|ku|mu)\b/',$kata)){ // Cek Suffixes
				$__kata = preg_replace('/(kan|i|an|nya|ku|mu)\b/','',$kata);
			
				if($this->cekKamus($__kata)){
					return $__kata; // Jika ada balik
				}
			}
		}
		
		return $kataAsal;
	}

	public function deleteSuffix($kata)
	{
		$kataAsal = $kata;

		if(preg_match('/(kan)\b|(i|an)\b/',$kata)){ // Cek Suffixes
			$__kata = preg_replace('/(kan)\b|(i|an)\b/','',$kata);
			
			if($this->cekKamus($__kata)){
				return $__kata; // Jika ada balik
			}
		}

		return $kataAsal;
	}
}

/* kondisi
0. meng
1. meng + suf
2. meng(k)
3. meng(k) + suf
4. peng
5. peng + suf
6. peng(k)
7. peng(k) + suf
8. meny
9. meny + suf
10. peny
11. peny + suf
12. men
13. men + suf
14. men(t)
15. men(t) + suf
16. pen
17. pen + suf
18. pen(t)
19. pen(t) + suf
20. mem
21. mem + suf
22. mem(p)
23. mem(p) + suf
24. pem
25. pem + suf
26. pem(p)
27. pem(p) + suf
28. me
29. me + suf
30. pe
31. pe + suf
32. per + suf
33. per(r) + suf
34. ber
35. ber(r)
36. ber + suf
37. ber(r) + suf
38. ter
39. ter(r)
40. di|se
41. di + suf
42. se + suf
43. ke + suf
44. suf */