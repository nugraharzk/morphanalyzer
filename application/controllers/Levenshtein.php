<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Levenshtein extends CI_Controller {

    public function index()
	{
		$this->load->view('v_levenshtein');
    }
    
    public function cariJarak()
    {
        $result['input'] = $this->input->post('input');
        $compare = $this->M_Morph->getKata();

        $i = 0;
        $result['hasil'] = [];
        $result['data'] = false;
        foreach ($compare as $key) {
            // echo $key->katadasar;
            $bandingkan = levenshtein($result['input'],$key->katadasar);
            
            if ($bandingkan < 2) {
                $result['hasil'][$i] = $key; 
                $result['nilai'][$i] = $bandingkan;
                $i++;
                $result['data'] = true;
            }
        }
        
        $result['jumlah'] = $i;

        $this->load->view('v_rlevenshtein',$result);
    }

}